
//Write a recursive function pattern(n) which prints a number triangle.

#include<stdio.h>
int rows(int n);
int numLine(int n);
int main()
{
    int x;
    printf("A program to show a number triangle\n");
    printf("-----------------------------------\n\n");
    printf("Enter the number of rows= ");
    scanf("%d",&x);
    rows(x);

    return 0;
}

int numLine(int n)
{

    if(n>0)
    {
        printf("%d",n);

        return numLine(n-1);
    }


}

int rows(int n)
{
    if(n>0)
    {
        rows(n-1);
        numLine(n);
        printf("\n");
        return 0;
    }
}




