
// Write a recursive function fibonacciSeq(n) to print the fibonacci sequence up to n.

#include<stdio.h>

int numPlace=0;
int fibonacciNumber(int n);
int fibonacciSeq(int z);

int main()
{
    int x,fib;
    printf("A program to find a fibonacci sequence upto a given number\n");
    printf("-------------------------------------------------------\n\n");
    printf("Enter a number to find the fibonacci sequence = ");
    scanf("%d",&x);
    fib= fibonacciSeq(x);


}

int fibonacciNumber(int n)
{
    if(n==0)
    {
        return 0;
    }
    else if (n==1)
    {
        return 1;
    }
    else
    {

        return fibonacciNumber(n-1)+fibonacciNumber(n-2);

    }
}



int fibonacciSeq(int z)
{
    if (numPlace<=z)
    {
        printf("%d\n", fibonacciNumber(numPlace));
        numPlace++;
        fibonacciSeq(z);
    }
}





